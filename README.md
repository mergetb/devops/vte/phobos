# Phobos

A simple virtual testbed facility.

![](img/diagram.png)

## tl;dr

**NOTE: Phobos is a development environment, and as such the default configuration
assumes a local registry ([user-config.yml](user-config.yml)), with all the
containers required to run Mars present. See [registry config](#registry-config)
to setup your local registry.**

## Registry Config

**NOTE:** If you have SELinux running you may see an error: `Error: open /dev/dma_heap: permission denied` when attempting to run podman with --privileged. You can allow podman unrestricted access using the following commands (also found in journald logs).

```
# ausearch -c 'podman' --raw | audit2allow -M my-podman
# semodule -X 300 -i my-podman.pp
```

### Initial setup

```shell
#!/bin/bash

sudo mkdir -p /var/lib/registry

sudo podman run \
    --privileged -d \
    --name registry \
    -p 5000:5000 \
    -v /var/lib/registry:/var/lib/registry \
    --restart=always registry:2
```

### Starting after workstation reboot

```shell
#!/bin/bash

sudo podman start registry
```

### Populating

From the Mars repo

```
source use_local_registry.sh
ymk build.yml
ymk containers.yml
ymk push-containers.yml
```

## Create portal account and commission phobos facility

**NOTE**: If you use a different account credentials than:
  - username: `murphy`
  - password: `mergetb`
  
when commissioning the facility, you will need to do either of the following:
  - Update the credentials in [user-config.yml](user-config.yml#L29-31) and then [regenerate the facility install](#installation-config)
  - Update the credentials directly in [merge-install/mars-config.yml](merge-install/mars-config.yml#L82-84)
```
# create account
mrg config set server grpc.mergetb.example.net
mrg config set port 443
mrg register murphy murphy@email.com -p mergetb

# activate account through portal ops 
#   - $opspw comes from the portal-genconf file: see here if using portal appliance:
#     https://gitlab.com/mergetb/portal/appliance/-/blob/master/launch-appliance.sh#L18
mrg login ops -p $opspw --nokeys
mrg init murphy
mrg activate murphy

# (optional: make murphy a portal admin; not something to do for general users, but can aid in debugging things in the VTE)
mrg update user admin murphy

mrg logout

mrg login murphy -p mergetb
mrg new facility phobos api.phobos.example.com model/cmd/phobos.xir merge-install/apiserver.pem
```

## Create the virtual facility

As root, do the following:

```
rvn build
rvn deploy
rvn pingwait ops mgmt
rvn status
rvn configure

# This step will likely take ~10 minutes, give or take depending on the quality of your Internet connection
ansible-playbook -i .rvn/ansible-hosts -i ansible-interpreters bootstrap.yml

# This will run in the foreground of your terminal. Leave it running forever
rvn apiserver
```

### Verifying installation

The ansible playbook above will take several minutes to complete. During this time, you may find it useful to use the virt-manager program to monitor the various nodes and switches as they install. Specific things to be aware of:

- Switches `xp` and `infra` occasionally fail to install due to an error in the Cumulus/ONIE install process. You can see this by observing the console for those nodes through virt-manager (see image below). If this happens, simply force reset them through virt-manager to restart the install process until it works
![Cumulus Install Errors](img/cumulus-error.png)

- The `ifr` node fetches boot contents from the `ops` node over the virtualized management network. You will see this node reboot a couple of times before coming up with the Merge FCOS image

- Once the `ifr` node installs, you can connect to it and verify that the core Mars service are up and running:

From your host, first ssh through `ops`:
```
eval $(rvn ssh ops)
ssh ifr
```

Then, check the mars service containers:
```
sudo podman ps
```

You should see something like the following:
```
[ops@ifr ~]$ sudo podman ps
CONTAINER ID  IMAGE                                                 COMMAND               CREATED        STATUS            PORTS       NAMES
c0061ef46057  172.22.0.1:5000/mergetb/facility/mars/cniman:main     /bin/sh -c /usr/b...  3 minutes ago  Up 3 minutes ago              mars-cniman
c100023d9cf2  172.22.0.1:5000/mergetb/facility/mars/apiserver:main  /bin/sh -c /usr/b...  3 minutes ago  Up 3 minutes ago              mars-apiserver
a7624482893a  172.22.0.1:5000/mergetb/facility/mars/metal:main      /bin/sh -c /usr/b...  3 minutes ago  Up 3 minutes ago              mars-metal
8c9ab8bb0770  172.22.0.1:5000/mergetb/facility/mars/wireguard:main                        3 minutes ago  Up 3 minutes ago              mars-wireguard
68d0f4efac93  docker.io/library/traefik:v2.10                       traefik --configF...  2 minutes ago  Up 3 minutes ago              mars-reverseproxy
593d4088bb9f  172.22.0.1:5000/mergetb/facility/mars/infrapod:main   /bin/sh -c /usr/b...  3 minutes ago  Up 3 minutes ago              mars-infrapod
e54c5ad1ba54  quay.io/coreos/etcd:v3.4.14                           /usr/local/bin/et...  3 minutes ago  Up 3 minutes ago              mars-etcd
c3874c166d1a  docker.io/minio/minio:RELEASE.2021-01-16T02-19-44Z    minio server /min...  3 minutes ago  Up 3 minutes ago              mars-minio
cc08148e0cda  quay.io/mergetb/frr:solovni                                                 2 minutes ago  Up 2 minutes ago              mars-frr
```

Note that you will typically see a failed systemd service when you login to the `ifr` node as root:
```
Failed Units: 1
  mars-canopy.service
```

This is expected. You will eventually need to restart the service which we indicate below

## Initialize the facility with MARS
Now continue with the setup

```
# ssh onto the ops node
eval $(rvn ssh ops)
```

Then, 
```
# initialize minio
mrs init minio

###
# at this point, you likely need to restart the mars-canopy service
ssh ifr "sudo systemctl restart mars-canopy"

# initialize the facility configuration system
mrs init config

# initialize the harbor
mrs init harbor

# !!!
# !!! Wait for harbor to initialize
# !!!
# !!! Once you see the container 'harbor.system.marstb-minio' on the ifr node:

# initialize image repository with EFI capable images
mrs init images -t v1.0.1 -i efi

# check network status
mrs list net
```

The testbed nodes `x[0-3]` should be network booting into the Merge Fedora36 hypervisor OS. However, they are likely to be sitting at a UEFI shell at this point:
![UEFI shell](img/hypervisor-uefi.png)

When you initialize the harbor (`mrs init harbor`) above, a bunch of things happen, including:
1. The testbed nodes `[x0-3]` are power cycled and attempt to PXE network boot
1. A Merge service called `sled` is created on `ifr`, which is responsible for servicing these network boots.

In `phobos`, it is likely that `x[0-3]` attempt to network boot before `sled` starts running. This is because rebooting virtual nodes is much quicker than pulling podman images and creating the harbor infrapod pod where `sled` runs. Note that this similar race condition does exist in real testbed facilities, however, it will typically not manifest because the time it takes to power cycle physical hardware is much greater than for virtual hardware.

So, the practical upshot is that you likely need to power cycle each of `x[0-3]` via virt-manager.

Once these nodes boot up, you now have an operational virtual testbed facility!

## Other Notes

### Clean slate restart

Sometimes things go wrong and you want to blow it all away and restart. This deletes the whole facility:

```
rvn destroy
```

### Installation Config

You may sometimes want to regenerate the installation artifacts (what gets generated in `merge-install`); for example:
- if a new `facility-install` version is released with updates or bug fixes
- if you change something in `user-config.yml`, such as container image tags
- you change something in the [phobos XIR model](model/topo.go)

To regenerate:

```
./generate-install-config.sh
```

(note that this script pulls `facility-install` from the main branch of [that repository](https://gitlab.com/mergetb/facility/install))

### MinIO notes

To use the MinIO client from Ops, do the following. If you change the credentials in your [user-config.yml](https://gitlab.com/mergetb/devops/vte/phobos/-/blob/ebf46e35ef1216e180bfffa0de726ccf0ee3c583/user-config.yml#L78-79), you'll need to reflect those here:

```
curl -OL https://dl.min.io/client/mc/release/linux-amd64/mc
chmod +x mc
./mc alias set mars http://minio:9000 mars muffins1234
./mc ls mars/
```

### Rebuilding Model

```
cd model/cmd
go build -o phobos
./phobos save # (generates phobos.xir)
./phobos save -j # (generates phobos.json)
```

### Installing etcd for debugging

```
ETCD_VER=v3.4.16

# choose either URL
DOWNLOAD_URL=https://storage.googleapis.com/etcd

rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
rm -rf /tmp/etcd-download-test && mkdir -p /tmp/etcd-download-test

curl -L ${DOWNLOAD_URL}/${ETCD_VER}/etcd-${ETCD_VER}-linux-amd64.tar.gz -o /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz
tar xzvf /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz -C /tmp/etcd-download-test --strip-components=1
rm -f /tmp/etcd-${ETCD_VER}-linux-amd64.tar.gz

cp /tmp/etcd-download-test/etcdctl /usr/bin/etcdctl
```

then

```
ETCDCTL_ENDPOINTS=etcd:2379 etcdctl get --prefix --keys-only ""
```
