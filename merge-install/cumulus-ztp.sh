#!/bin/bash
function error() {
  echo -e "ERROR: The ZTP script failed while running the command $BASH_COMMAND at line $BASH_LINENO." >&2
  exit 1
}

# Log all output from this script
exec >> /var/log/autoprovision 2>&1
date "+%FT%T ztp starting script $0"

trap error ERR

# Set timezone
timedatectl set-timezone 'America/Los_Angeles'

# Set ops username
if ! id ops > /dev/null 2>&1; then
	yes | adduser --disabled-password ops
	usermod -aG sudo ops
fi

# Set ops password
usermod --password $(echo 'ops' | openssl passwd -1 -stdin) ops

# Install ops pubkey in ops user
mkdir -vp /home/ops/.ssh
if [ ! -f /home/ops/.ssh/authorized_keys ] || ! grep -q 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1dGQ7QRJXS5W10lUo0gNQfwtKlr7SFtPBd7C53hGWHco/5Y5wHeN+ewcBICl8aynV9kh6bqrC3hsD+8NjTwBNTVjkNa1iUDPBpYRzQaYIKqcdGYxhBRskK530nEh+duuQjVnVdfGRyskPz8u0JKpM/wEzvNqH5BiuAs1V/YYGhxyT7aFde48HzX1H9KAqPwwevGWBU2Spag7V6MJQEgY1oa86zpHe+qCrQ4JI5HVEMWjQvAzBDcfAkCPJe2Xq2y5V6tf9D/t//gT22lAjuI5JIhkB0+aO9WpMPIml1nuQocYYOVeNwFSnJ1GVHSKPJQ9lQzOh7nQcmDoMRH0G1RIf' /home/ops/.ssh/authorized_keys; then
	echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1dGQ7QRJXS5W10lUo0gNQfwtKlr7SFtPBd7C53hGWHco/5Y5wHeN+ewcBICl8aynV9kh6bqrC3hsD+8NjTwBNTVjkNa1iUDPBpYRzQaYIKqcdGYxhBRskK530nEh+duuQjVnVdfGRyskPz8u0JKpM/wEzvNqH5BiuAs1V/YYGhxyT7aFde48HzX1H9KAqPwwevGWBU2Spag7V6MJQEgY1oa86zpHe+qCrQ4JI5HVEMWjQvAzBDcfAkCPJe2Xq2y5V6tf9D/t//gT22lAjuI5JIhkB0+aO9WpMPIml1nuQocYYOVeNwFSnJ1GVHSKPJQ9lQzOh7nQcmDoMRH0G1RIf >> /home/ops/.ssh/authorized_keys
fi
chmod 0600 /home/ops/.ssh/authorized_keys
chown -R ops:ops /home/ops/.ssh

# Install ops pubkey in root user
mkdir -vp /root/.ssh
if [ ! -f /root/.ssh/authorized_keys ] || ! grep -q 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1dGQ7QRJXS5W10lUo0gNQfwtKlr7SFtPBd7C53hGWHco/5Y5wHeN+ewcBICl8aynV9kh6bqrC3hsD+8NjTwBNTVjkNa1iUDPBpYRzQaYIKqcdGYxhBRskK530nEh+duuQjVnVdfGRyskPz8u0JKpM/wEzvNqH5BiuAs1V/YYGhxyT7aFde48HzX1H9KAqPwwevGWBU2Spag7V6MJQEgY1oa86zpHe+qCrQ4JI5HVEMWjQvAzBDcfAkCPJe2Xq2y5V6tf9D/t//gT22lAjuI5JIhkB0+aO9WpMPIml1nuQocYYOVeNwFSnJ1GVHSKPJQ9lQzOh7nQcmDoMRH0G1RIf' /root/.ssh/authorized_keys; then
	echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC1dGQ7QRJXS5W10lUo0gNQfwtKlr7SFtPBd7C53hGWHco/5Y5wHeN+ewcBICl8aynV9kh6bqrC3hsD+8NjTwBNTVjkNa1iUDPBpYRzQaYIKqcdGYxhBRskK530nEh+duuQjVnVdfGRyskPz8u0JKpM/wEzvNqH5BiuAs1V/YYGhxyT7aFde48HzX1H9KAqPwwevGWBU2Spag7V6MJQEgY1oa86zpHe+qCrQ4JI5HVEMWjQvAzBDcfAkCPJe2Xq2y5V6tf9D/t//gT22lAjuI5JIhkB0+aO9WpMPIml1nuQocYYOVeNwFSnJ1GVHSKPJQ9lQzOh7nQcmDoMRH0G1RIf >> /root/.ssh/authorized_keys
fi
chmod 0600 /root/.ssh/authorized_keys

# Allow password-less sudo for ops user
if ! (grep -q 'ops  ALL=(ALL) NOPASSWD: ALL' /etc/sudoers); then
	echo 'ops  ALL=(ALL) NOPASSWD: ALL' | tee -a /etc/sudoers > /dev/null
fi

mkdir -vp /certs

cat << EOF > /certs/apiserver.pem
-----BEGIN CERTIFICATE-----
MIIByTCCAW6gAwIBAgIBATAKBggqhkjOPQQDAjAvMRAwDgYDVQQKEwdNZXJnZVRC
MRswGQYDVQQDExJwaG9ib3MuZXhhbXBsZS5jb20wHhcNMjIwMjE3MjMwODMxWhcN
MjYwMjE2MjMwODMxWjAvMRAwDgYDVQQKEwdNZXJnZVRCMRswGQYDVQQDExJwaG9i
b3MuZXhhbXBsZS5jb20wWTATBgcqhkjOPQIBBggqhkjOPQMBBwNCAASALVDzzxP9
/Hex9W94D447lws/ULVPdLd9YJxEXr4jrDQot2QKv8byXaxOJEVgkrdn+FoLa/Z7
N5Dcglg83CApo3sweTAOBgNVHQ8BAf8EBAMCBaAwEwYDVR0lBAwwCgYIKwYBBQUH
AwEwDAYDVR0TAQH/BAIwADBEBgNVHREEPTA7gglhcGlzZXJ2ZXKCFm9wcy5waG9i
b3MuZXhhbXBsZS5jb22CFmFwaS5waG9ib3MuZXhhbXBsZS5jb20wCgYIKoZIzj0E
AwIDSQAwRgIhAKS4NtuFhDKfQTTYBnuuTe/UoHCMWoGpOU6LlXR3J2sjAiEAsvMu
ldjuzlZvDv5tMvwQWE928ykLvpDAN0wshsb6Pl8=
-----END CERTIFICATE-----

EOF

cat << EOF > /certs/apiserver-key.pem
-----BEGIN EC PRIVATE KEY-----
MHcCAQEEIBXI1EwFCG4du7JAwK7A04g3qPDNQ0ZMbTiuFpvRLjfOoAoGCCqGSM49
AwEHoUQDQgAEgC1Q888T/fx3sfVveA+OO5cLP1C1T3S3fWCcRF6+I6w0KLdkCr/G
8l2sTiRFYJK3Z/haC2v2ezeQ3IJYPNwgKQ==
-----END EC PRIVATE KEY-----

EOF

### Canopy

systemctl stop canopy@mgmt || true

# Determine arch
arch=$(uname -m)
case $arch in
    armv7l)
      ## GOARM=7 canopy build issues some problematic instructions on armv7l switches with cumulus stock kernel,
      ## so fallback to v5 build
      ##   https://github.com/golang/go/issues/18483
      curl http://ground-control/canopy.arm5 -o /usr/local/bin/canopy
      ;;

    x86_64)
      curl http://ground-control/canopy -o /usr/local/bin/canopy
      ;;

    *)
      echo unknown architecture: $arch
      exit 1
      ;;
esac

chmod +x /usr/local/bin/canopy

cat << EOF > /lib/systemd/system/canopy@.service
[Unit]
Description=Canopy service
After=network-online.target
Wants=network-online.target

[Service]
ExecStartPre=/usr/bin/test -f /var/run/frr/watchfrr.started
ExecStart=/usr/local/bin/canopy
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
EOF

grep -qxF 'canopy' /etc/vrf/systemd.conf || echo 'canopy' >> /etc/vrf/systemd.conf

# start frr before canopy (issue #22)
sed -i 's/^bgpd=no/bgpd=yes/g' /etc/frr/daemons
systemctl restart frr

systemctl daemon-reload
systemctl enable canopy@mgmt
systemctl start canopy@mgmt

# CUMULUS-AUTOPROVISIONING
exit 0
