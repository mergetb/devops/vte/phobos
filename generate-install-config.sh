#!/bin/bash

set -ex

curl -L https://gitlab.com/mergetb/facility/install/-/jobs/artifacts/main/raw/build/facility-install?job=make \
     -o facility-install

chmod +x facility-install

./facility-install generate model/cmd/phobos.xir
