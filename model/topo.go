package phobos

import (
	"log"

	xir "gitlab.com/mergetb/xir/v0.3/go"
	. "gitlab.com/mergetb/xir/v0.3/go/build"
)

func Topo() *xir.Facility {

	tb, err := NewBuilder(
		"phobos",
		"phobos.example.com",
	)
	if err != nil {
		log.Fatalf("new builder: %v", err)
	}

	// Infrapod Server
	ifr := tb.Infraserver("ifr",
		Ens(1, Gbps(1), Gw()), // raven
		Ens(1, Gbps(1), Mgmt()),
		Ens(1, Gbps(1), Infranet()),
		Ens(1, Gbps(1)),
		Disks(1, Sata3(), SysDisk()),
		Disks(1, Sata3(), EtcdDisk()),
		Disks(1, Sata3(), MinioDisk()),
		RoleAdd(xir.Role_BorderGateway),
		RoleAdd(xir.Role_Gateway),
		RoleAdd(xir.Role_InfrapodServer),
		Raven("172.22.0.1"),
		Product("Qemu", "Infrapod Server", ""),
	)
	// Name the gateway link
	ifr.NICs[0].Ports[0].Name = "ens1"

	ifr.NICs[1].Ports[0].Mac = "04:70:00:00:00:04"
	ifr.NICs[2].Ports[0].Mac = "04:71:00:00:00:04"
	ifr.NICs[3].Ports[0].Mac = "04:72:00:00:00:04"

	// Storage Server
	stor := tb.StorageServer("stor",
		Ens(1, Gbps(1), Mgmt()),
		Ens(1, Gbps(1), Infranet()),
		Disks(1, Sata3(), SysDisk()),
		Disks(2, GB(20), Sata3(), RallyDisk()),
		RoleAdd(xir.Role_RallyHost),
		Raven("172.22.0.1"),
		Product("Qemu", "Storage Server", ""),
	)
	stor.NICs[0].Ports[0].Mac = "04:70:00:00:00:05"
	stor.NICs[1].Ports[0].Mac = "04:71:00:00:00:05"

	emu := tb.NetworkEmulator("emu",
		Ens(1, Gbps(1), Mgmt()),
		Ens(1, Gbps(1), Infranet()),
		Ens(1, Gbps(1), Xpnet()),
		Disks(1, Sata3(), SysDisk()),
		Raven("172.22.0.1"),
		Product("Qemu", "Network Emulator", ""),
	)
	emu.NICs[0].Ports[0].Mac = "04:70:00:00:00:06"
	emu.NICs[1].Ports[0].Mac = "04:71:00:00:00:06"
	emu.NICs[2].Ports[0].Mac = "04:72:00:00:00:06"

	nodes := tb.Nodes(4, "x",
		Procs(1, Cores(2)),
		Dimms(1, GB(4)),
		Disks(1, Sata3(), SysDisk()),
		Disks(2, Sata3(), MarinerDisk(), GB(10)),
		Ens(1, Gbps(1), Mgmt()),
		Ens(1, Gbps(1), Infranet()),
		Ens(1, Gbps(1), Xpnet()),
		RoleAdd(xir.Role_Hypervisor),
		AllocModes(
			xir.AllocMode_Physical,
			xir.AllocMode_Virtual,
		),
		DefaultImage("bullseye"),
		Rootdev("/dev/sda"),
		Uefi(),
		Raven("172.22.0.1"),
		Product("Qemu", "Testbed Node", ""),
	)

	// mgmt MACs
	nodes[0].NICs[0].Ports[0].Mac = "04:70:00:00:00:10"
	nodes[1].NICs[0].Ports[0].Mac = "04:70:00:00:00:11"
	nodes[2].NICs[0].Ports[0].Mac = "04:70:00:00:00:12"
	nodes[3].NICs[0].Ports[0].Mac = "04:70:00:00:00:13"

	// infranet MACs
	nodes[0].NICs[1].Ports[0].Mac = "04:71:00:00:00:10"
	nodes[1].NICs[1].Ports[0].Mac = "04:71:00:00:00:11"
	nodes[2].NICs[1].Ports[0].Mac = "04:71:00:00:00:12"
	nodes[3].NICs[1].Ports[0].Mac = "04:71:00:00:00:13"

	// xpnet MACs
	nodes[0].NICs[2].Ports[0].Mac = "04:72:00:00:00:10"
	nodes[1].NICs[2].Ports[0].Mac = "04:72:00:00:00:11"
	nodes[2].NICs[2].Ports[0].Mac = "04:72:00:00:00:12"
	nodes[3].NICs[2].Ports[0].Mac = "04:72:00:00:00:13"

	// Switches
	mgmt := tb.MgmtLeaf("mgmt",
		Eth(1, Gbps(1), Mgmt()),
		Swp(10, Gbps(1), Mgmt()),
		RoleAdd(xir.Role_Stem),
		Product("Qemu", "Mgmt Leaf", ""),
	)

	infra := tb.InfraLeaf("infra",
		Eth(1, Gbps(1), Mgmt()),
		Swp(7, Gbps(1), Infranet()),
		RoleAdd(xir.Role_Stem),
		Product("Qemu", "Infra Leaf", ""),
	)
	infra.NICs[0].Ports[0].Mac = "04:70:00:00:00:01"

	xp := tb.XpLeaf("xp",
		Eth(1, Gbps(1), Mgmt()),
		Swp(5, Gbps(1), Xpnet()),
		RoleAdd(xir.Role_Stem),
		Product("Qemu", "Xp Leaf", ""),
	)
	xp.NICs[0].Ports[0].Mac = "04:70:00:00:00:02"

	// mgmt connectivity
	swps := mgmt.NICs[1].Ports
	tb.Connect(swps[0], infra.NextEth())
	tb.Connect(swps[1], xp.NextEth())
	tb.Connect(swps[3], ifr.NICs[1].Ports[0])
	tb.Connect(swps[4], stor.NextEns())
	tb.Connect(swps[5], emu.NextEns())
	for idx, x := range nodes {
		tb.Connect(swps[6+idx], x.NextEns())
	}

	// infra connectivity
	tb.Connect(infra.NextSwp(), ifr.NICs[2].Ports[0])
	tb.Connect(infra.NextSwp(), stor.NextEns())
	tb.Connect(infra.NextSwp(), emu.NextEns())
	for _, x := range nodes {
		tb.Connect(infra.NextSwp(), x.NextEns())
	}

	// xp connectivity
	tb.Connect(xp.NextSwp(), emu.NextEns())
	for _, x := range nodes {
		tb.Connect(xp.NextSwp(), x.NextEns())
	}

	return tb.Facility()

}
