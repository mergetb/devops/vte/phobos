package main

import (
	phobos "gitlab.com/mergetb/devops/vte/phobos/model"
	"gitlab.com/mergetb/xir/v0.3/go/build"
)

func main() {
	build.Run(phobos.Topo())
}
