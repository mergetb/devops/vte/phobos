/* The Phobos Merge Virtual Site
 * =============================
 *
 */

topo = {
    name: "phobos_"+Math.random().toString().substr(-6),
    nodes: [
        ops(),
        infraserver(),
        ...["x0", "x1", "x2", "x3"].map(x => tbnode(x)),
        moa(),
        stor(),
    ],
    switches: [
        cumulus("mgmt"),
        onie("infra"),
        onie("xp"),
    ],
    links: [
        v2v("infra", 1, "mgmt", 1,  { mac: { infra: '04:70:00:00:00:01' } }),
        v2v("xp",    1, "mgmt", 2,  { mac: { xp:    '04:70:00:00:00:02' } }),
        v2v("ops",   1, "mgmt", 3,  { mac: { ops:   '04:70:00:00:00:03' } }),
        v2v("ifr",   1, "mgmt", 4,  { mac: { ifr:   '04:70:00:00:00:04' }, boot: 20 }),
        v2v("stor",  1, "mgmt", 5,  { mac: { stor:  '04:70:00:00:00:05' }, boot: 20 }),
        v2v("emu",   1, "mgmt", 6,  { mac: { emu:   '04:70:00:00:00:06' }, boot: 20 }),
        v2v("x0",    1, "mgmt", 7,  { mac: { x0:    '04:70:00:00:00:10' } }),
        v2v("x1",    1, "mgmt", 8,  { mac: { x1:    '04:70:00:00:00:11' } }),
        v2v("x2",    1, "mgmt", 9,  { mac: { x2:    '04:70:00:00:00:12' } }),
        v2v("x3",    1, "mgmt", 10, { mac: { x3:    '04:70:00:00:00:13' } }),

        v2v("ifr",  2, "infra", 2, { mac: { ifr:  '04:71:00:00:00:04' } }),
        v2v("stor", 2, "infra", 3, { mac: { stor: '04:71:00:00:00:05' } }),
        v2v("emu",  2, "infra", 4, { mac: { emu:  '04:71:00:00:00:06' } }),
        v2v("x0",   2, "infra", 5, { mac: { x0:   '04:71:00:00:00:10' }, boot: 5 }),
        v2v("x1",   2, "infra", 6, { mac: { x1:   '04:71:00:00:00:11' }, boot: 5 }),
        v2v("x2",   2, "infra", 7, { mac: { x2:   '04:71:00:00:00:12' }, boot: 5 }),
        v2v("x3",   2, "infra", 8, { mac: { x3:   '04:71:00:00:00:13' }, boot: 5 }),

        v2v("emu", 3, "xp", 2, { mac: { emu: '04:72:00:00:00:06' } }),
        v2v("x0",  3, "xp", 3, { mac: { x0:  '04:72:00:00:00:10' } }),
        v2v("x1",  3, "xp", 4, { mac: { x1:  '04:72:00:00:00:11' } }),
        v2v("x2",  3, "xp", 5, { mac: { x2:  '04:72:00:00:00:12' } }),
        v2v("x3",  3, "xp", 6, { mac: { x3:  '04:72:00:00:00:13' } }),

        Link("ifr", 3, '', 0, { external: true, mac: { ifr: '04:72:00:00:00:04' } } ),
    ]
}

function ops() {
    return {
        name: "ops",
        image: 'centos-8stream',
        cpu: { cores: 2 },
        memory: { capacity: GB(4) },
        defaultdisktype: { dev: 'sda', bus: 'sata' },
    };
}

function infraserver() {
    return {
        name: "ifr",
        defaultnic: 'e1000',
        defaultdisktype: { dev: 'sda', bus: 'sata' },
        image: 'netboot',
        os: 'netboot',
        cpu: { cores: 4 },
        memory: { capacity: GB(4) },
        firmware: '/usr/share/edk2/ovmf/OVMF_CODE.fd',
        disks: [
            { size: "5G", dev: 'sdb', bus: 'sata' },
            { size: "20G", dev: 'sdc', bus: 'sata' },
        ],
    };
}

function moa() {
    return {
        name: "emu",
        notestnet: true,
        defaultnic: 'e1000',
        defaultdisktype: { dev: 'sda', bus: 'sata' },
        image: 'netboot',
        os: 'netboot',
        cpu: { cores: 2, passthru: true }, // needed for click
        memory: { capacity: GB(16) },
        firmware: '/usr/share/edk2/ovmf/OVMF_CODE.fd',
    };
}

function stor() {
    return {
        name: "stor",
        notestnet: true,
        defaultnic: 'e1000',
        defaultdisktype: { dev: 'sda', bus: 'sata' },
        image: 'netboot',
        os: 'netboot',
        cpu: { cores: 2 }, 
        memory: { capacity: GB(16) },
        firmware: '/usr/share/edk2/ovmf/OVMF_CODE.fd',
        disks: [
            { size: "20G", dev: 'sdb', bus: 'sata' },
            { size: "20G", dev: 'sdc', bus: 'sata' },
        ],
    };
}

function cumulus(name) {
    return {
        name: name,
        image: 'cumulusvx-4.2',
        cpu: { cores: 2 },
        memory: { capacity: GB(2) },

        // needed for Fedora 35 because the node doesn't boot with newer Q35 chipsets
        // see https://gitlab.com/mergetb/devops/vte/phobos/-/issues/5
        machine: 'pc-q35-5.2', // default from Fedora 34
    };
}

function onie(name) {
    return {
        name: name,
        defaultnic: 'e1000', // due to virtio + v2v checksum issues
        notestnet: true,
        image: 'onie-x86-bios',
        cpu: { cores: 2 },
        memory: { capacity: GB(2) },
    };
}

function tbnode(name) {
    return {
        name: name,
        notestnet: true,
        defaultnic: 'e1000',
        defaultdisktype: { dev: 'sda', bus: 'sata' },
        image: 'netboot',
        os: 'netboot',
        cpu: { cores: 2, model: 'host', passthru: true },
        memory: { capacity: GB(4) },
        firmware: '/usr/share/edk2/ovmf/OVMF_CODE.fd',
        disks: [
            { size: "10G", dev: 'sdb', bus: 'sata' },
            { size: "10G", dev: 'sdc', bus: 'sata' },
        ],
    };
}

function v2v(a, ai, b, bi, props={}) {
    lnk = Link(a, ai, b, bi, props);
    lnk.v2v = true;
    return lnk;
}
