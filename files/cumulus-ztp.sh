#!/bin/bash

function error() {
  echo -e "ERROR: The ZTP script failed while running the command $BASH_COMMAND at line $BASH_LINENO." >&2
  exit 1
}

# Log all output from this script
exec >> /var/log/autoprovision 2>&1
date "+%FT%T ztp starting script $0"

trap error ERR

mkdir -p /root/.ssh
cat >> /root/.ssh/authorized_keys <<EOF
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3e0frRD6el0aK+kh6Ba4/Mc3pYs99BBVWf8pw3yMO31wR6+l/uscSu15oJGxbfsZknqZKKVfD925q03wcblzA3z0Q9F6uPb+gK+/q8+xWU9yzDKNF7zjqdzVuP+6E2xQv48pfAxu1/m3bqQHL1jLIbJG2V+53WaaCx+RG8OmyxPH0MVHzX2ajtpDP1K6/fKr+wCefxJ2yo6WRRh/95KlhaxLNPNql3xJno3HIYd3x2+hR0hajtPHM6Gj2LORVUXui8M0ZaO/NWeyy3mcK3KTk2rkKlztuIFUyp7Z5L+wPzp6MEgDaQjoOEZXEM7WwqbeSNYv/2pQlOlZcTyYhRuTR ry@mirror
EOF

chmod 0600 /root/.ssh/authorized_keys

# CUMULUS-AUTOPROVISIONING
exit 0
