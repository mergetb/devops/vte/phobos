#!/bin/bash

ign=merge-install/ifr.ign
nm=files/rvnlink-ens1.nmconnection

patch_nm=true
patch_hostname=true
patch_hosts=true

# Don't patch if it's already there
files=$(jq -r ".storage.files[].path" $ign)
for f in ${files[@]}; do
	if [ "$f" = "/etc/NetworkManager/system-connections/rvnlink-ens1.nmconnection" ]; then
		echo "nm already patched"
		patch_nm=false
	elif [ "$f" = "/etc/hostname" ]; then
		echo "hostname already patched"
		patch_hostname=false
	elif [ "$f" = "/etc/hosts" ]; then
	    echo "/etc/hosts already patched"
		patch_hosts=false
	fi
done

# Add NetworkManager config for rvn link (ens1)
if [ "$patch_nm" = true ]; then
	nm64=$(base64 -w 0 $nm)
	jq ".storage.files |= . + [{
		\"group\": {},
		\"overwrite\": false,
		\"path\": \"/etc/NetworkManager/system-connections/rvnlink-ens1.nmconnection\",
		\"user\": {},
		\"contents\": {
		  \"source\": \"data:text/plain;charset=utf-8;base64,$nm64\",
		  \"verification\": {}
		},
		\"mode\": 384
		}]" $ign > tmp.json
	mv tmp.json $ign
fi

# Add hostname
if [ "$patch_hostname" = true ]; then
	hostname64=$(echo ifr.phobos.example.com | base64 -w 0 -)
	jq ".storage.files |= . + [{
		\"group\": {},
		\"overwrite\": true,
		\"path\": \"/etc/hostname\",
		\"user\": {},
		\"contents\": {
		  \"source\": \"data:text/plain;charset=utf-8;base64,$hostname64\",
		  \"verification\": {}
		},
		\"mode\": 420
		}]" $ign > tmp.json
	mv tmp.json $ign
fi

# Add hosts
if [ "$patch_hosts" = true ]; then
	hosts64=$(printf "\n192.168.126.10 grpc.mergetb.example.net\n192.168.126.10 api.mergetb.example.net\n" | base64 -w 0 -)
	jq ".storage.files |= . + [{
		\"group\": {},
		\"path\": \"/etc/hosts\",
		\"append\": [{
		  \"source\": \"data:text/plain;charset=utf-8;base64,$hosts64\",
		  \"verification\": {}
		}],
		\"user\": {},
		\"mode\": 420
		}]" $ign > tmp.json
	mv tmp.json $ign
fi
