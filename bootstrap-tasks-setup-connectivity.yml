---

- name: "get phobos network"
  ansible.builtin.shell:
    cmd: virsh net-list | grep -e 'phobos_[0-9]*_raven-infra' | awk {'print $1'}
  register: shell_cmd

- name: "set fact phobos_network"
  ansible.builtin.set_fact:
    phobos_network: "{{ shell_cmd.stdout }}"

- block:
  - name: "end play if phobos network is empty"
    fail:
      msg: "phobos network is missing, should be 'phobos_[0-9]*_raven-infra'. re-run `rvn deploy`?"
  - meta: end_play
  when: phobos_network == ""

- name: "get phobos interface"
  ansible.builtin.shell:
    cmd: virsh net-info {{ phobos_network }} | grep Bridge | awk {'print $2'}
  register: shell_cmd

- name: "set fact phobos_interface"
  ansible.builtin.set_fact:
    phobos_interface: "{{ shell_cmd.stdout }}"

- block:
  - name: "end play if phobos interface is empty"
    fail:
      msg: "phobos interface is missing, should be 'virbr[0-9]*'. re-run `rvn deploy`?"
  - meta: end_play
  when: phobos_interface == ""

# XXX these iptables rules are not persistent
# TODO can we use `firewall-cmd --zone=libvirt --add-forward` on F34+ yet?
#   <https://github.com/firewalld/firewalld/pull/613>
#
# BJK: the above does not work because the libvirt bridges operate in 'nat' mode
# which creates iptables rules to reject forwarding with "icmp unreachable" messages; e.g.:
# iptables --list-rules | grep tt0 | grep unreach
# -A LIBVIRT_FWI -o tt0 -j REJECT --reject-with icmp-port-unreachable
# -A LIBVIRT_FWO -i tt0 -j REJECT --reject-with icmp-port-unreachable
#
# The approach here is to manually install higher priority rules in the FORWARD chain
# which match before the libvirt chains above kick in

# sudo iptables -I FORWARD -o tt0 -i virbr<m> -j ACCEPT
- name: "add iptables: phobos_interface -> tt0"
  ansible.builtin.iptables:
    chain: FORWARD
    out_interface: "tt0"
    in_interface: "{{ phobos_interface }}"
    jump: "ACCEPT"
    action: "insert"
    rule_num: 1

# sudo iptables -I FORWARD -i tt0 -o virbr<m> -j ACCEPT
- name: "add iptables: tt0 -> phobos_interface"
  ansible.builtin.iptables:
    chain: FORWARD
    out_interface: "{{ phobos_interface }}"
    in_interface: "tt0"
    jump: "ACCEPT"
    action: "insert"
    rule_num: 1

- name: "get IP address of ops host"
  ansible.builtin.shell:
    cmd: (eval $(rvn ssh ops) hostname -I) | grep --word-regexp --only-matching -E "172\.22\.[0-9]+\.[0-9]+"
  register: shell_cmd

- name: "set fact phobos_interface"
  ansible.builtin.set_fact:
    ops_ip: "{{ shell_cmd.stdout }}"

- block:
  - name: "end play if ops_ip is empty"
    fail:
      msg: "ops_ip is missing, should be '172.22.x.x'"
  - meta: end_play
  when: ops_ip == ""

# also replaces any old ops_ip entry
- name: add ops_ip entry to /etc/hosts
  ansible.builtin.lineinfile:
    path: /etc/hosts
    state: present
    regexp: '^172\.22\.[0-9]+\.[0-9]+ phobos.example.com api.phobos.example.com'
    line: "{{ ops_ip }} phobos.example.com api.phobos.example.com"

# clear local resolver cache
- name: send SIGHUP to to clear dnsmasq resolver cache
  ansible.builtin.shell: killall -s HUP dnsmasq
  ignore_errors: yes

- name: test connectivity by pinging api.phobos.example.com
  ansible.builtin.shell:
    cmd: ping -c1 api.phobos.example.com
  register: shell_output
  until: shell_output.rc == 0
  retries: 5
  delay: 5
